package jdbcConnector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Query {
	static Connection conn;

	public static boolean login(String username,String password){
		boolean hasLogin=false;
		try {
			conn=getConnection();
			Statement sta = conn.createStatement();
			ResultSet rs=sta.executeQuery("select * from userlogin(\'"+username+"\',\'"+password+"\');");
			rs.next();
			hasLogin=rs.getBoolean(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hasLogin;
	}
	
	public static void register(String username,String password){

		try {
			conn=getConnection();
			Statement sta = conn.createStatement();
			sta.executeQuery("select register(\'"+username+"\',\'"+password+"\');");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection() throws SQLException {

		if (conn == null) {
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "postgres");
		}
		return conn;
	}
}
